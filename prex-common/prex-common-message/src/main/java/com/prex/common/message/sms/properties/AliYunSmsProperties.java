package com.prex.common.message.sms.properties;

import lombok.Data;

/**
 * @Classname AliSmsProperties
 * @Description 阿里短信配置
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-27 09:23
 * @Version 1.0
 */
@Data
public class AliYunSmsProperties extends BaseSmsProperties{

    /**
     * 短信类型
     */
    private String type = "aliyun";

}
